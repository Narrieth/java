package practica;

/**
 *
 * @author narrieth
 */
public class EstructuraRepetitivaWhile1 {
    public static void main(String[]ar){
        int x=2;
        while(x<=100){
            System.out.print(x);
            System.out.print(" - ");
            x = x+2;
        }
    }
}
