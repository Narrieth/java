/*
Escribir un programa que solicite ingresar 10 notas de alumnos
y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos 
menores.
 */
package practica;
import java.util.Scanner;
/**
 *
 * @author narrieth
 */
public class EstrucutraREpetitivaWhile5 {
    public static void main(String[]ar){
        Scanner teclado = new Scanner(System.in);
        int x, nota, mayor, menor;
        x=1;
        mayor=0;
        menor=0;
        while(x<=10){
            System.out.print("Ingrese nota: ");
            nota=teclado.nextInt();
            if(nota>=7){
                mayor = mayor +1;
            }else{
                menor = menor+1;
            }
            x=x+1;
        }
        System.out.print("cantidad de alumnos con notas mayores o iguales a 7: ");
        System.out.println(mayor);
        System.out.print("Cantidad de alumnos con notas menores a 7: ");
        System.out.println(menor);
    }
}
