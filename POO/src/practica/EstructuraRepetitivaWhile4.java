/*
Una planta que fabrica perfiles de hierro posee un lote de n piezas.
Confeccionar un programa que pida ingresar por teclado la cantidad de 
piezas a procesar y luego ingrese la longitud de cada perfil; sabiendo
que la pieza cuya longitud esté comprendida en el rango de 1,20 y 1,30
son aptas. Imprimir por pantalla la cantidad de piezas aptas que hay en el lote.
 */
package practica;

import java.util.Scanner;
/**
 *
 * @author narrieth
 */
public class EstructuraRepetitivaWhile4 {
    public static void main(String[]ar){
        Scanner teclado = new Scanner(System.in);
        int x, cantidad, n;
        float largo;
        x=1;
        cantidad=0;
        System.out.print("Cuantas piezas procesara: ");
        n = teclado.nextInt();
        while(x<=n){
            System.out.print("ingrese la medida de las piezas: ");
            largo = teclado.nextFloat();
            if(largo>=1.20 && largo <=1.30){
                cantidad = cantidad +1;
            }
            x=x+1;
        }
        System.out.print("la cantidad de piezas aptas son: ");
        System.out.println(cantidad);
        
    }
}
