package practica;

import java.util.Scanner;

/**Escribir un programa que solicite la carga de un valor positivo y nos muestre desde 1 hasta el valor ingresado de uno en uno.
Ejemplo: Si ingresamos 30 se debe mostrar en pantalla los números del 1 al 30.**/
/** @author narrieth */

public class EstructuraRepetitivaWhile2 {
    public static void main(String[]ar){
        Scanner teclado = new Scanner(System.in);
        int n,x;
        System.out.print("Ingrese el valor de n: ");
        n = teclado.nextInt();
        x = 1;
        while(x<=n){
            System.out.print(x);
            System.out.print(" - ");
            x = x+1;
        }
    }
}
