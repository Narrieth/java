/*
Desarrollar un programa que permita la carga de 10 valores por 
teclado y nos muestre posteriormente la suma de los valores ingresados y su promedio.
 */
package practica;
import java.util.Scanner;
/**
 *
 * @author narrieth
 */
public class EstructuraRepetitivaWhile3 {
    public static void main(String[]ar){
        Scanner teclado = new Scanner(System.in);
        int x, suma, valor, promedio;
        x = 1;
        suma=0;
        while(x<=10){
            System.out.print("ingrese 10 valores: ");
            valor = teclado.nextInt(); 
            suma = suma+valor;
            x=x+1;
        }
        promedio=suma/10;
        System.out.print("La suma de los 10 valores es: ");
        System.out.println(suma);
        System.out.print("El promedio de la suma es: ");
        System.out.println(promedio);

    }
}
