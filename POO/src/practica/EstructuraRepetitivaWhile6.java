/*
Se ingresan un conjunto de n alturas de personas por teclado. 
Mostrar la altura promedio de las personas.
 */
package practica;

import java.util.Scanner;
/**
 *
 * @author narrieth
 */
public class EstructuraRepetitivaWhile6 {
    public static void main(String[]ar){
        Scanner teclado = new Scanner(System.in);
        int x, n;
        float altura, suma, promedio;
        System.out.print("Cuantas personas hay: ");
        n= teclado.nextInt();
        x=1;
        suma=0;
        while(x<=n){
            System.out.print("ingrese la altura: ");
            altura= teclado.nextFloat();
            suma=suma + altura;
            x=x+1;
        }
        promedio=suma/n;
        System.out.print("Altura promedio: ");
        System.out.println(promedio);
    }
}
